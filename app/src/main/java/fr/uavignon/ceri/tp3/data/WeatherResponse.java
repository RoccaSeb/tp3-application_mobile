package fr.uavignon.ceri.tp3.data;

import java.util.List;

public class WeatherResponse {
    public final Wind wind = null;
    public final List<Condition> weather = null;
    public final Clouds clouds = null;
    public final Main main = null;
    public final Long dt = null;

    public static class Clouds
    {
        public final int all = 0;
    }

    public static class Main
    {
        public final Float temp = null;
        public final Float feels_like = null;
        public final Float temp_min = null;
        public final Float temp_max = null;
        public final Integer pressure = null;
        public final Integer humidity = null;
    }

    public static class Wind {
        public final Float speed = null;
        public final Integer deg = null;
    }

    public static class Condition {
        public final String description = null;
        public final String icon = null;
    }



}