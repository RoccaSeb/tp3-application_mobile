package fr.uavignon.ceri.tp3.data;

import android.util.Log;

import fr.uavignon.ceri.tp3.DetailFragment;

public class WeatherResult {
    //public final boolean isLoading;
    //public final List<Forecast>
    //public final List<Weather> forecasts;
    //public final Throwable error;
    private City cityInfo;
    //public final WeatherResponse w;


    static void transferInfo(WeatherResponse weatherInfo, City cityInfo)
    {
        String TAG = WeatherResult.class.getSimpleName();

        //Log.d(TAG,  weatherInfo.main.temp.toString());
        cityInfo.setTemperature(weatherInfo.main.temp);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setWindSpeed(Math.round(weatherInfo.wind.speed));
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setTempKelvin(weatherInfo.main.temp);



    }

    /* WeatherResult(boolean isLoading, WeatherResponse weatherRep, Throwable error, City city) //List<Forecast> forecasts
    {
        this.isLoading = isLoading;
        this.w = weatherRep;
        this.error = error;
        //transferInfo(weatherRep, city);
    } */

}
