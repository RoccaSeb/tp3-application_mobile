package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private WeatherRepository repository;
    private MutableLiveData<City> city;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public DetailViewModel (Application application) {
        super(application);
        repository = WeatherRepository.get(application);
        city = new MutableLiveData<>();
        isLoading = repository.getIsLoadingRepo();
        webServiceThrowable = repository.getWebServiceThrowableRepo();
    }

    public void setCity(long id) {
        repository.getCity(id);
        city = repository.getSelectedCity();
    }
    MutableLiveData<City> getCity() {
        return city;
    }

    public void update(City city) { repository.loadWeatherCity(city);}



    public LiveData<Boolean> getIsLoadingModel()
    {
        return isLoading;
    }

    public LiveData<Throwable> getWebServiceThrowableModel()
    {
        return webServiceThrowable;
    }

    public void eraseWebServiceThrowableModel()
    {
        repository.eraseWebServiceThrowableRepo();
        webServiceThrowable = repository.getWebServiceThrowableRepo();
    }


}

